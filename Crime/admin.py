from django.contrib import admin
from .models import Categorie
#admin.site.register(Categorie)

@admin.register(Categorie)
class CategorieAdmin(admin.ModelAdmin):
    list_display = ["type","number",]
    list_filter = ["type", "date",]
    search_fields = ("type",)